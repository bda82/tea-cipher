#-*- coding: utf-8 -*-

from datetime import datetime
from . import db
from app.exceptions import ValidationError


class Element(db.Model):
    """
    Doc: Main Element class
    """

    # заполнение модели элемента

    # параметр имени таблицы для хранения элементов
    __tablename__ = 'elements'
    # поле идентификатора
    id = db.Column('id', db.Integer, primary_key=True)
    # поле заголовка элемента
    title = db.Column(db.String(50))
    # поле содержимого элемента
    body = db.Column(db.String)
    # поле признака-отметки
    done = db.Column(db.Boolean)
    # поле даты и времени (временной метки) создания элемента
    publication_date = db.Column(db.DateTime)


    def __init__(self, title, body):
        """
        Doc: reinit. class constructor
        """
        # начальная инициализация данных элемента
        self.title = title
        self.body = body
        self.done = False
        self.publication_date = datetime.utcnow()


    @staticmethod
    def from_json(json_post):
        """
        Doc: convert data from JSON object
        """
        # получение заголока
        title = json_post.get('title')
        # получение содержимого
        body = json_post.get('body')
        # если содержимого нет или поле просто пустое
        if body is None or body == '':
            # вызов ошибки
            raise ValidationError('element does not have a body')

        # если все нормально - возврат объекта элемента
        return Element(title,body)


    def to_json(self):
        """
        Doc: convert data to JSON object
        """
        # создание объекта JSON и заполнение его данными из параметров класса
        element_json = {
            'id' : self.id,
            'title' : self.title,
            'body' : self.body,
            'done' : self.done
            }

        # возврат объекта JSON
        return element_json


    def __repr__(self):
        """
        Doc: get string object with full info
        """
        # представление заголовка в виде тега
        return "<element '%s'>" % self.title
