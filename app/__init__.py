#-*- coding: utf-8 -*-

from flask import Flask
from flask import render_template
from flask.ext.bootstrap import Bootstrap
from flask.ext.moment import Moment
from flask.ext.sqlalchemy import SQLAlchemy
from config import config

# создание объекта для верстки Twitter Bootstrap
bootstrap = Bootstrap()

# создание объекта для доступа к библиотеке Flask-Moment для работы с moment.js
moment = Moment()

# создание объекта для базы данных
db = SQLAlchemy()

# создание приложения
def create_app(config_name):
    """
    Doc: Create Flask elements application
    """

    # создание основного объекта
    app = Flask(__name__)
    # создание конфигурации приложения из объекта конфигурации
    app.config.from_object(config[config_name])
    # сохранениее текущего имени приложения
    config[config_name].init_app(app)

    # инциализация инструментов Twitter Bootstrap для приложения
    bootstrap.init_app(app)
    # инициализация инструментов Moment для приложения
    moment.init_app(app)
    # инициализация объекта базы данных для приложения
    db.init_app(app)

    # загрузка шаблонов
    from app.main import main as main_blueprint
    # регистрация шаблонов в приложении
    app.register_blueprint(main_blueprint)

    # возврат инициализированного объекта приложения
    return app
