#-*- coding: utf-8 -*-

import re
import threading
import unittest
from selenium import webdriver
from app import create_app, db
from app.models import Element


class IntegrationTestCase(unittest.TestCase):
    """
    Doc: Test Case for Application
    """

    # первичный клиент
    client = None

    @classmethod
    def setUpClass(cls):
        # запуск Firefox
        try:
            # если есть
            cls.client = webdriver.Firefox()
        except:
            # если его нет
            pass

        # если существует клиент
        if cls.client:
            # конфигурация тестовая - загрузка приложения
            cls.app = create_app('testing')
            # загрузка контекста
            cls.app_context = cls.app.app_context()
            # запуск временного контекста
            cls.app_context.push()

            # удаление данных из базы
            db.drop_all()
            # создание базы заново
            db.create_all()
            # создание нового элемента
            element = Element(title='title1', body='body1')
            # создание объекта в базе данных
            db.session.add(element)
            # сохранение сессии
            db.session.commit()

            # запуск сервера с контекстом с потоке
            threading.Thread(target=cls.app.run).start()


    @classmethod
    def tearDownClass(cls):
        """
        Doc: class for testing, run onse at the end of testing
        """
        # если есть слиент
        if cls.client:
            # закрыть клиент тестирования
            cls.client.close()
            # удалить базу данных
            db.drop_all()
            # удалить сессию
            db.session.remove()
            # закрыт и вытолкнуть контекст
            cls.app_context.pop()


    def setUp(self):
        """
        Doc: run once on the start of test case
        """
        # если нет клента
        if not self.client:
            # пропустить тест - нет браузета и его API
            self.skipTest('Web browser not available')

    def tearDown(self):
        """
        Doc: testcase run ones at the end of test case
        """
        pass


    def test_home_page(self):
        """
        Doc: test home page
        """
        # получить страницу
        self.client.get('http://localhost:5000/')
        # тест: сравнение с
        self.assertTrue(re.search('Diploma2017', self.client.page_source))

    def test_new_page(self):
        """
        Doc: test new element page
        """
        # получить страницу
        self.client.get('http://localhost:5000/')
        # получить элемент HTML
        self.client.find_element_by_link_text('New element').click()
        # тест: сравить по Истина/Лож
        self.assertTrue('Back to list' in self.client.page_source)
        # найти элемент title и заполнить
        self.client.find_element_by_name('title').send_keys('SelTitle')
        # найти элемент body и заполнить
        self.client.find_element_by_name('body').send_keys(u'Содержимое элемента')
        # найти элемент submit и "нажать"
        self.client.find_element_by_name('submit').click()
        # тест: сравить по Истина/Лож
        self.assertTrue(re.search('SelTitle', self.client.page_source))
