#-*- coding: utf-8 -*-

import unittest
from flask import url_for
from app import create_app, db
from app.models import Element

class TestAPIs(unittest.TestCase):
    """
    Doc: class for API testing
    """
    def setUp(self):
        """
        Doc: code, which calls once as object create
        """
        # создать объект
        self.app = create_app('testing')
        # создать контекст объекта
        self.app_context = self.app.app_context()
        # загрузить объект
        self.app_context.push()
        # создать базу
        db.create_all()
        # вызвать основной тест
        self.client = self.app.test_client()

    def tearDown(self):
        """
        Doc: code, which calls once as object destroy
        """
        # удать сессию после теста
        db.session.remove()
        # удалить таблицу
        db.drop_all()
        # вытолкнуть контекст
        self.app_context.pop()

    def get_username(self, username):
        """
        Doc: get username test
        """
        # если мы в тестовом режиме
        if username == 'test':
            password = 'test'
            # все ОК
            return True
        # что-то не так
        return None

    def test_404(self):
        """
        Doc: test 404 errors
        """
        # передать неправильный маршрут
        response = self.client.get('/some-wrong/url')
        # тест: сравнение с кодом 404
        self.assertTrue(response.status_code == 404)

    def test_no_auth(self):
        """
        Doc: test no authenticated user acion
        """
        # получить элемент
        response = self.client.get(
            '/elements/api/v1.0/elements',
            content_type='application/json'
            )
        # тест: сравнение с кодом 403
        self.assertTrue(response.status_code == 403)
