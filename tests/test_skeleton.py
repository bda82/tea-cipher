#-*- coding: utf-8 -*-

import unittest
from flask import current_app
from app import create_app, db


class SkeletonTestCase(unittest.TestCase):
    """
    Doc: test case sceleton for application
    """
    def setUp(self):
        """
        Doc: run onse on app test start
        """
        # создание объекта тестирования
        self.app = create_app('testing')
        # создание контекста
        self.app_context = self.app.app_context()
        # загрузка контекста
        self.app_context.push()
        # создание таблицы
        db.create_all()

    def tearDown(self):
        """
        Doc: run onse on app test complete
        """
        # удаление сессии
        db.session.remove()
        # удаление базы
        db.drop_all()
        # выгрузка контекста
        self.app_context.pop()

    def test_app_exists(self):
        """
        Doc: test is app is exist
        """
        # тест: сравнение, существует ли приложение
        self.assertFalse(current_app is None)

    def test_app_is_testing(self):
        """
        Doc: test if app alreary testing
        """
        # сравнение признака тестирования в конфигурации приложения
        self.assertTrue(current_app.config['TESTING'])
