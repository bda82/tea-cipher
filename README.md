Дипломная работа по защите персональных данных
==============================================
Серверная часть

Установка
=========
```
$ python manage.py createall
```

Запуск
======
```bash
$ python manage.py runserver
```

Тестирование
============
```bash
$ python manage.py test
```

Удаление
========
```bash
$ python manage.py dropall
```


Взаимодействие с приложением по сети Интернет
=============================================
**Получение списка записей**
```
curl -u test:test -H "Accept: application/json" -i http://localhost:5000/elements/api/v1.0/element
```

**Получение отдельной записи**
```
curl -u test:test -H "Accept: application/json" -i http://localhost:5000/elements/api/v1.0/element/<ID>
```

**Размещение записи**
```
curl -u test:test -H "Content-Type: application/json" -X POST -d '{"title":"Личные данные", "body":"Мои личные данные"}' -i http://localhost:5000/elements/api/v1.0/element/create
```

**Обновление записи**
```
curl -u test:test -H "Content-Type: application/json" -X PUT -d '{"title":"Мой пароль", "body":"Мой пароль от почтового ящика"}' -i http://localhost:5000/elements/api/v1.0/element/update/<ID>
```

**Удаление записи**
```
curl -u test:test -H "Accept: application/json" -X DELETE http://localhost:5000/elements/api/v1.0/element/delete/<ID>
```
