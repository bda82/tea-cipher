#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
from app import create_app
from app import db
from app.models import Element
from flask.ext.script import Manager
from flask.ext.script import Shell

# создание приложения
app = create_app(os.getenv('FLASK_CONFIG') or 'default')

# создание основного объекта обработчика запросов
manager = Manager(app)


def make_shell_context():
    """
    Doc: make shell
    """
    # вернуть объекты приложения, базы и первого элемента
    return dict(app=app, db=db, Element=Element)

# добавление команды оболочки
manager.add_command("shell", Shell(make_context=make_shell_context))


@manager.command
def test():
    """
    Doc: Run the unit tests.
    """
    # подключить модуль тестирования (системный)
    import unittest
    # создать объект тестирования и загрузить его
    tests = unittest.TestLoader().discover('tests')
    # передать управление запущенному тесту
    unittest.TextTestRunner(verbosity=2).run(tests)


@manager.command
def createall():
  """
  Doc: Сreats the table
  """
  db.create_all()

@manager.command
def dropall():
  """
  Doc: Drop the table
  """
  db.drop_all()

# точка запуска приложения
if __name__ == '__main__':
    # запуск приложения с базовыми параметрами
    manager.run()
